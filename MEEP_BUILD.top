# -*- python -*-

# Top-level meep build file.
# Copyright (c) 2019 Chris Sloan

import os
import variants.host_posix
import variants.stm32l432
import variants.xmc4700

Import(
        'base_env',
        'host_variant',
        'meep_def_cross_variant',
        'meep_def_host_variant',
        )

if GetOption('verbose') >= 2:
    # Show information during MEEP_BUILD processing (as opposed to
    # during the biuld phase).
    print('Processing %s' % os.getcwd())
    print('    PLATFORM: %s' % base_env['PLATFORM'])


######################################################################
# Define build variants and evaluate subsidiary MEEP_BUILDs for them.
######################################################################

# The meep_def_host_variant() and meep_def_cross_variant() functions will
# evaluate src/MEEP_BUILD and its subsidiary MEEP_BUILDS.  As such, this file
# does not need to do so explicitly.

# Host builds: For local executables and unit tests.
meep_def_host_variant(
    host_variant,
    env_customize = variants.host_posix.custom_env,
)

# Embedded builds.
meep_def_cross_variant(
    "stm32l432", "stm32", "arm",
    env_customize = variants.stm32l432.custom_env,
)

meep_def_cross_variant(
    "xmc4700", "xmc", "arm",
    env_customize = variants.xmc4700.custom_env,
)


#EOF
